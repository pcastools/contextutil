// Contextutil defines utilities to simplify working .

/*
This software is distributed under an MIT license. You should have received a copy of the license along with this software. If not, see <https://opensource.org/licenses/MIT>.
*/

package contextutil

import (
	"context"
	"sync"
)

// link is used to temporarily link a context with a cancel function.
type link struct {
	sync.Mutex
	doneC    chan struct{} // Channel controlling a background go-routine
	finished bool          // Are we finished?
}

// Link is used to temporarily link a context with a cancel function.
type Link interface {
	Stop() // Stop breaks the link.
}

/////////////////////////////////////////////////////////////////////////
// Link functions
/////////////////////////////////////////////////////////////////////////

// NewLink will temporarily link the given context to the given cancel function: that is, the context firing will cause the cancel function to be called. Call Stop on the returned Link to break the link; for example:
//	defer NewLink(ctx, cancel).Stop()
func NewLink(ctx context.Context, cancel context.CancelFunc) Link {
	return NewChannelLink(ctx.Done(), cancel)
}

// NewChannelLink will temporarily link the given channel to the given cancel function: that is, the channel firing will cause the cancel function to be called. Call Stop on the returned Link to break the link; for example:
//	defer NewChannelLink(doneC, cancel).Stop()
func NewChannelLink(c <-chan struct{}, cancel context.CancelFunc) Link {
	l := &link{
		doneC: make(chan struct{}),
	}
	go func() {
		select {
		case <-l.doneC:
		case <-c:
			l.Lock()
			defer l.Unlock()
			if !l.finished {
				cancel()
			}
		}
	}()
	return l
}

// Stop breaks the link between the context and cancel function.
func (l *link) Stop() {
	close(l.doneC)
	l.Lock()
	defer l.Unlock()
	l.finished = true
}
